# Thea API Integration

[[_TOC_]]

[Contact Us](mailto:thea-support@eliiza.com.au)

## Overview

### What is Thea

Thea is a document classification and information extraction API.

Our solution harnesses cloud services combined with custom-built Machine Learning models to automatically extract
business critical data accurately from documents. With the help of Thea, your business can speed up processes and make
accurate predictions. Thea processes a document and returns the extracted information and its confidence of the
extraction.

Some **benefits** Thea provides are

- Improves operational efficiency
- Secure and highly available platform
- Intelligent document processing
- Provides frictionless customer experience

### How Thea works

Each document passes through a preprocessing pipeline to ensure best practice inputs for text extraction. Optical
character recognition (OCR) then extracts data from your documents. This data is passed to Thea’s natural language
processing (NLP) models for document classification, field identification and extraction. Document input and data
consumption are facilitated by our secure, scalable REST API.

### Labelling

Thea provides an easy-to-use web interface to label documents. The tool helps with identifying raw data and adding
informative labels to provide context. The training data collected from the documents is fed in to our machine learning
models to help train the model. The model prediction success is derived from the availability of quality labelled data
in the form of a training dataset fed to the algorithm - better the training data is, the better the model performs.

**Access and permissions setup**

The labeller will need cross-account access to the following storage locations within your environment

1. Read access to the location where the orginal documents to be labelled and trained for the use-case are stored
2. Write access to the location where the training data exported from the labeller are stored. This typically contains -

- Result of the OCR/Textract
- Images of each page
- Annotation file

### Training

The dataset produced from the labeller tool, refered to as the training data, helps the machine learning models learn
and make predictions.

**Access and permissions setup**

Cross account location access should be given to our training account so that it has permission to read the files from the
location mentioned above. Time limited signed URL access is a method by which access can be further limited. The
training job starts the sagemaker account, gets all the data from the location and trains on all of the data mentioned above in
the output location.

## Getting started with Thea

### Use Case Discovery

Thea supports document classification and/or field extraction on a number of document types including passports,
payslips and invoices. It can also be trained for custom documents and use cases as well, so get in touch with us to
discuss how Thea can help with your specific use case.

### Authentication and Authorisation

The Thea team will work closely with you to provide the necessary credentials to securely authenticate and access the
model endpoints.

### Technical Documentation

Technical documentation can be found [here](./tech.md)

## Recommendations/ Best Practices

### Native PDF vs Image scans as PDF

Native PDF documents, i.e. ones that are all digital and have been created by a computer and not photo scanned will have
an advantage over images (.jpeg) or pdfs containing images.

PDFs have the ability to be all digit text or contain a large image of a scan of a document.

There is much less ambiguity around small details (such as the difference between a comma (`,`) and a dot(`.`) when they
do not go through image scanning.

### Document Rotation

Thea supports documents that are rotated, but will perform best when they align with to the edge.

Thea does not currently support document scans with text in different rotations on the same page. Examples of this
include:

- two sheets of paper, each 90 degrees to each other on the one scanned image.
- passports where the back-page and face-page are both on the one scan. Back pages often contain text and are at
  90degrees to the passport details on the next page.

Our recommended approach is to limit the image to just one orientation. For a passport, this would be enough to crop the
image to focus on the details page, without including the rotated back-page.

### Document Size and number of pages

The number of pages and size of the documents (bytes) have an effect on the ability to processed. thea has timeouts
that will be triggered if the document is too large to be processed in one API call. This might mean that a 20-page
document or one with high-resolution image details may need to be broken-up before being sent to thea.

For the best performance, our service works best when the document has 10 pages or less and a maximum size of 5MB. 

Talk to us about your dataset and we can help with recommended approaches to solve this.

## Frequently Asked Questions

### What file formats are supported by thea?

- JPEG/JPG Image
- BMP Image
- PNG Image
- TIFF Image
- BMP Image
- PDF Document. Native PDF directly from a PDF generator or a PDF containing an image scan

### How many documents does thea need to train the model for a particular use case?

While there is no 'one-size-fits-all' approach to this question, there are some general best practices we recommend. Our solution works best when it is fed a representative sample of what your production system would be exposed to. We could start with a random sampling size of 100-200 documents that include some popular templates and see what categories appear within the dataset. After analysing the samples, we may then ask for more documents to help better train our models. 

### What does a good training document sample look like?

We understand that you might not always be able to source high quality documents, but for us to train and refine our models we would ideally like -
- A single document on the one page (i.e. for example not having an image of a passport and a residence card on the same page)
- High-enough resolution with no distortion, reflection or obscuring information 

### What if I have PII data in my dataset and I want to anonymise it before sending it to thea for training?

A possible solution to dealing with PII and to protect individual identification is to anonymise sensitive data before sending it to us for model training. 

Our recommended approach is 
- Step 1. Identify the data to be anonymised - This could be an individual's name, credit card details, mobile numbers etc.
- Step 2. Use software tools to mask PII data 
- Step 3. Replace the original PII data with text that is as close to the original format as possible (example - similar font, size and format (date, numeric, string...)) 

Please do not redact data (i.e. replace PII text in documents with black boxes) because then the model wouldn't know the type of data fields to train against.

### Why am I not getting a response back?

see Document Size section

see Document Rotation section

### Why is the API request timing out?

see Document Size section

### Why am I not getting a particular field extracted from my form?

Is this a field that Thea has been trained to extract. If you are unsure, please get in touch.
