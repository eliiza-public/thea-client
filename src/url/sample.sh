export THEA_API_KEY=XXX
export THEA_USER_ID=XXX
export THEA_USER_SECRET=XXX

export FILENAME="/path/to/file/test.jpeg"

AUTH_TOKEN=$(curl -s -X POST --user $THEA_USER_ID:$THEA_USER_SECRET \
    --header 'Content-Type: application/x-www-form-urlencoded' \
    https://thea.eliiza.ai/auth | jq '.access_token' | tr -d '"')


curl --header "Authorization: $AUTH_TOKEN" \
  --header "x-api-key: $THEA_API_KEY" \
  --data-binary @$FILE https://thea.eliiza.ai/classify | jq '.'
