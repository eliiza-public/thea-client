[< Back to main](../../README.md)

## Commandline / Shell Script

### Dependencies

- examples are in unix shell format
- (optionally) make use of a json formatter [jq](https://stedolan.github.io/jq/)

### Running

1. [Download Script](sample.sh)
2. edit the variables at the top of the script. For authorisation:

```bash
export THEA_API_KEY=XXX
export THEA_USER_ID=XXX
export THEA_USER_SECRET=XXX
```

And this one to specify a file to send

```bash
export FILENAME="/path/to/file/test.jpeg"
```

3. Uncomment one of the ENDPOINTS to either do `classify` or `passport` field extraction for example.
4. The example makes use of a json formatter to format the output
 
On a mac

```bash
brew install jq
```

Once installed, the script will use jq to format your output

### Notes

1. Any spaces in the filename need to be escaped by surrounding the filename with quotes (e.g. do FILE_NAME="example
   doc.pdf"
   instead of FILE_NAME=example doc.pdf)
1. If you've copied and pasted the curl commands from a text editor, make sure any quotes are proper quotes (e.g.
   Microsoft Word might change your quotes to typographer quotes which will be incorrect for bash, if this is the case
   then just backspace the typographer quotes and explicitly type a quotation mark in it's place)
1. Make sure the path to your file is correct. If you're not sure, then navigate to the directory the file is located in
   and just provide the filename without a path

[< Back to main](../../README.md)
