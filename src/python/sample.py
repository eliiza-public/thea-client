import argparse
import base64
import json
from pathlib import Path
import requests

parser = argparse.ArgumentParser()
parser.add_argument('-f', '--file', type=str, required=True)
parser.add_argument('-k', '--key', type=str, required=True)
parser.add_argument('-i', '--thea_user_id', type=str, required=True)
parser.add_argument('-s', '--thea_user_secret', type=str, required=True)
args = parser.parse_args()

# Get the bytes of the file to send to the API:
filepath = Path(args.file)
file_bytes = filepath.read_bytes()

thea_url = "https://thea.eliiza.ai"
auth_url = "https://thea.eliiza.ai/auth"

auth = base64.b64encode(f"{args.thea_user_id}:{args.thea_user_secret}".encode('utf-8')).decode('ascii')

auth_response = requests.post(url=auth_url,
                              headers={
                                  'Authorization': f"Basic {auth}",
                                  'Content-Type': 'application/x-www-form-urlencoded'})

auth_token = json.loads(auth_response.text)['access_token']

# Send the file bytes to the CLASSIFY endpoint:
classify_response = requests.post(
    url=f'{thea_url}/classify',
    data=file_bytes,
    headers={
        'x-api-key': args.key,
        'Authorization': auth_token
    }
)

print(json.loads(classify_response.text))

