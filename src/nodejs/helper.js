const https = require('https');

module.exports = function httpsRequest(options, payload) {
    return new Promise((resolve, reject) => {
        const request = https.request(options, (response) => {
            let body = [];
            response.on('data', function (chunk) {
                body.push(chunk);
            });
            response.on('end', function () {
                try {
                    body = Buffer.concat(body).toString();
                } catch (e) {
                    reject(e);
                }
                resolve(body);
            });
        });
        request.on('error', (e) => {
            reject(e.message);
        });
        request.write(payload);
        request.end();
    });
}
