const fs = require('fs');
const path = require('path');

const httpsRequest = require('./helper');

// This gets the url and key from the variables in run.sh, but
// replace process.env.hostname, process.env.key and process.env.filename
// with your values to run directly from this file
const FILENAME = process.env.filename
const HOSTNAME = process.env.hostname
const KEY = process.env.key

// Get the file bytes to send to the API
const fileBytes = fs.readFileSync(FILENAME);

const data = JSON.stringify({
    filename: path.basename(FILENAME),
    b64: fileBytes.toString('base64')
});

const preprocessOptions = {
    hostname: HOSTNAME,
    path: '/document/preprocess',
    method: 'POST',
    headers: {
        'x-api-key': KEY,
        'Content-Type': 'application/json'
    }
};
const modelOptions = {
    hostname: HOSTNAME,
    path: '/document/invoice',
    method: 'POST',
    headers: {
        'x-api-key': KEY
    }
};

// Send the document to be preprocessed.
httpsRequest(preprocessOptions, data).then((protoDocument) => {
    // Send the document to the extraction endpoint to get the field values
    httpsRequest(modelOptions, protoDocument).then((results) => {
        console.log(results);
    });
});
