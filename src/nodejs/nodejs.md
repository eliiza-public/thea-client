[< Back to main](../../README.md)

## Node.js <a class='anchor' id='node-call-api'></a>

Run classification and field extraction using the run script. Make sure you change the FILENAME and KEY before running
the script.

```sh
# ../nodejs/run.sh


FILENAME=../example.pdf
HOSTNAME=thea.com
KEY=XXXXX

filename=$FILENAME hostname=$HOSTNAME key=$KEY node classification.js

filename=$FILENAME hostname=$HOSTNAME key=$KEY node field_extraction.js
```

The classification and field extraction examples use this https helper function to make the POST requests:

```js
// ../nodejs/helper.js

const https = require('https');

module.exports = function httpsRequest(options, payload) {
     return new Promise((resolve, reject) => {
        const request = https.request(options, (response) => {
            let body = [];
            response.on('data', function(chunk) {
                body.push(chunk);
            });
            response.on('end', function() {
                try {
                    body = Buffer.concat(body).toString();
                } catch(e) {
                    reject(e);
                }
                resolve(body);
            });
        });
        request.on('error', (e) => {
          reject(e.message);
        });
        request.write(payload);
        request.end();
    });
}

```

### Document Classification <a class='anchor' id='node-classification'></a>

```js
// ../nodejs/classification.js

const fs = require('fs');
const path = require('path');

const httpsRequest = require('./helper');

// This gets the url and key from the variables in run.sh, but
// replace process.env.hostname, process.env.key and process.env.filename
// with your values to run directly from this file
const FILENAME = process.env.filename
const HOSTNAME = process.env.hostname
const KEY = process.env.key

// Get the file bytes to send to the API
const fileBytes = fs.readFileSync(FILENAME);

const data = JSON.stringify({
    filename: path.basename(FILENAME),
    b64: fileBytes.toString('base64')
});

const preprocessOptions = {
    hostname: HOSTNAME,
    path: '/document/preprocess',
    method: 'POST',
    headers: {
        'x-api-key': KEY,
        'Content-Type': 'application/json'
    }};
const modelOptions = {
    hostname: HOSTNAME,
    path: '/document/classify',
    method: 'POST',
    headers: {
        'x-api-key': KEY
    }};

// Send the document to be preprocessed.
httpsRequest(preprocessOptions, data).then((protoDocument) => {
    // Send the document to be classified
    httpsRequest(modelOptions, protoDocument).then((results) => {
        console.log(results);
    });
});

```

### Field Extraction <a class='anchor' id='node-field-extraction'></a>

```js
// ../nodejs/field_extraction.js

const fs = require('fs');
const path = require('path');

const httpsRequest = require('./helper');

// This gets the url and key from the variables in run.sh, but
// replace process.env.hostname, process.env.key and process.env.filename
// with your values to run directly from this file
const FILENAME = process.env.filename
const HOSTNAME = process.env.hostname
const KEY = process.env.key

// Get the file bytes to send to the API
const fileBytes = fs.readFileSync(FILENAME);

const data = JSON.stringify({
    filename: path.basename(FILENAME),
    b64: fileBytes.toString('base64')
});

const preprocessOptions = {
    hostname: HOSTNAME,
    path: '/document/preprocess',
    method: 'POST',
    headers: {
        'x-api-key': KEY,
        'Content-Type': 'application/json'
    }};
const modelOptions = {
    hostname: HOSTNAME,
    path: '/document/invoice',
    method: 'POST',
    headers: {
        'x-api-key': KEY
    }};

// Send the document to be preprocessed.
httpsRequest(preprocessOptions, data).then((protoDocument) => {
    // Send the document to the extraction endpoint to get the field values
    httpsRequest(modelOptions, protoDocument).then((results) => {
        console.log(results);
    });
});

```

[< Back to main](../../README.md)
