# Thea API

[[_TOC_]]

This page will guide you through calling the Thea api. To get access to Thea, please
follow: [Introduction to Thea page](README.md)

## What is the Thea API

Thea is a document classification and information extraction API. It is a secure RESTful api.

## API Definition

The API has a Swagger definition file that you can peruse individual methods: 
- [Swagger Common](./swagger_common.json)
-  [Swagger IDP](./swagger_idp.json)

## Calling Thea

The following links give sample code to call Thea in various languages. Since Thea is a RESTful api, you can call it
with any client programming language. If you preferred language isn't included, either review the examples below or get
in touch with us.

* [Bash](src/url/bash.md)
* [Python](src/python/python.md)
* Node.js - coming soon

## Response format

Thea's classification and extraction endpoints return a JSON formatted response with the model specific information for
each page in the submitted document.

### Document Classification

For document classification, the endpoint returns the document type with the highest confidence. 
In this example it is an `invoice`.

An example JSON response for the document __classify__ endpoint. In this case, invoice is marked as the highest
confidence (98%) for the 1-and-only page in the document.

```json
{
  "pages": [
    {
      "value": "invoice",
      "confidence": 0.9821152687072754
    }
  ]
}
```

### Invoice Field Extraction

For field extraction, invoice in this case, the endpoint returns a list of the fields document types and the confidence. 
The results are sorted, with the highest
confident classification first. In this example it is an `invoice`.

Given a 1-page invoice, the JSON response for an __invoice__ endpoint looks like:

```json
{
  "pages": [
    {
      "receiver_name": {
        "value": "Demo Co",
        "confidence": 0.9
      },
      "receiver_address": {
        "value": "123 Elizabeth Street, Melbourne VIC 3124",
        "confidence": 0.86
      },
      "receiver_phone": {
        "value": "03 1234 1234",
        "confidence": 0.78
      },
      "receiver_email": {
        "value": "info@democo.com",
        "confidence": 0.92
      },
      "supplier_name": {
        "value": "John Co",
        "confidence": 0.82
      },
      "supplier_abn": {
        "value": "81123456",
        "confidence": 0.72
      },
      "supplier_address": {
        "value": "456 Turbot St, Brisbane QLD 4000",
        "confidence": 0.81
      },
      "supplier_email": {
        "value": "info@johnco.com",
        "confidence": 0.89
      },
      "supplier_phone": {
        "value": "07 3456 3456",
        "confidence": 0.73
      },
      "supplier_bsb": {
        "value": null,
        "confidence": 0
      },
      "supplier_account_number": {
        "value": null,
        "confidence": 0
      },
      "invoice_date": {
        "value": "21 Apr 2020",
        "confidence": 0.68
      },
      "invoice_id": {
        "value": "INV-1234",
        "confidence": 0.73
      },
      "total_tax_amount": {
        "value": "$12.12",
        "confidence": 0.78
      },
      "total_amount": {
        "value": "$133.32",
        "confidence": 0.76
      },
      "line_items": [
        {
          "description": {
            "value": "Hrs for services",
            "confidence": 0.56
          },
          "product_code": {
            "value": "A1",
            "confidence": 0.67
          },
          "quantity": {
            "value": "2",
            "confidence": 0.89
          },
          "unit": {
            "value": "$60.60",
            "confidence": 0.74
          },
          "amount": {
            "value": "$121.20",
            "confidence": 0.74
          }
        }
      ]
    }
  ]
}
```


